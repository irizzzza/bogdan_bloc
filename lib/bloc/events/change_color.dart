abstract class ColorEvent{
  final int color;

  ColorEvent(this.color);
}

class ChangeColorEvent extends ColorEvent {
  ChangeColorEvent(int color) : super(color);


}