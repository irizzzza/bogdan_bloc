import 'dart:async';

import 'package:bogdan_bloc/bloc/events/change_color.dart';

 class ColorBloc {
  int _index = 0;

  final _indexStateController = StreamController<int>.broadcast();

  StreamSink<int> get _updateIndex => _indexStateController.sink;

  Stream<int> get index => _indexStateController.stream;

  final _indexEventController = StreamController<ChangeColorEvent>();

  Sink<ColorEvent> get indexEventSink => _indexEventController.sink;

  ColorBloc() {
    _indexEventController.stream.listen(_mapEventToState);
  }


  void _mapEventToState(ColorEvent event) {
    if (event is ChangeColorEvent) {
      _index = event.color;
      print(_index);
    }
    _updateIndex.add(_index);
  }

  void dispose() {
    _indexStateController.close();
    _indexEventController.close();
  }
}