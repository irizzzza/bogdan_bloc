import 'package:bogdan_bloc/bloc/color_bloc.dart';
import 'package:bogdan_bloc/bloc/events/change_color.dart';
import 'package:bogdan_bloc/res/colors.dart';
import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {

  final _bloc;

  AppDrawer(this._bloc);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _bloc.index,
        initialData: 0,
        builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
          return Drawer(
            child: ListView.builder(
              itemCount: appColors.length,
              itemBuilder: (BuildContext ctx, int index) {
                return RaisedButton(
                  onPressed: () {
                    _bloc.indexEventSink.add(
                      ChangeColorEvent(index),
                    );
                    Navigator.of(context).pop();
                  },
                  color: appColors[index],
                );
              },
            ),
          );
        });
  }
}
