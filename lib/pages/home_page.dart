import 'package:bogdan_bloc/bloc/color_bloc.dart';
import 'package:bogdan_bloc/res/colors.dart';
import 'package:bogdan_bloc/widgets/app_drawer.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  final _bloc = ColorBloc();
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _bloc.index,
      initialData: 0,
      builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
        return Scaffold(
          appBar: AppBar(),
          backgroundColor: appColors[snapshot.data],
          body: Container(),
          drawer: AppDrawer(_bloc),
        );
      },
    );
  }

  void dispose() {
   // super.dispose();
    _bloc.dispose();
  }
}
